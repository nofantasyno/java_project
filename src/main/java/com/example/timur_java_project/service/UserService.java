package com.example.timur_java_project.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.example.timur_java_project.model.User;
import com.example.timur_java_project.web.dto.UserRegistrationDto;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User save(UserRegistrationDto registration);
}
