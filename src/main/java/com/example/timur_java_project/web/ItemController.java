package com.example.timur_java_project.web;

import com.example.timur_java_project.exception.ResourceNotFoundException;
import com.example.timur_java_project.model.Item;
import com.example.timur_java_project.repository.ItemRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
@Controller
public class ItemController {

    private ItemRepository itemRepository;

    @Autowired
    public ItemController(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    //redo
    @GetMapping("/dashboard")
    @ResponseBody
    public ModelAndView index (@RequestParam(value = "page", required = false, defaultValue = "0") int page, @RequestParam(value = "sort", required = false, defaultValue = "createdAt") String sort) {
        Pageable pageable = PageRequest.of(page, 4, Sort.by(sort));
        Page<Item> items = itemRepository.findAll(pageable);

        ModelAndView model = new ModelAndView("dashboard");
        int totalPages = items.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addObject("pageNumbers", pageNumbers);
        }
        model.addObject("items", items);
        return model;
    }

    /*@GetMapping("/items")
    @ResponseBody
    public ModelAndView getAllItems() {
        ModelAndView modelAndView = new ModelAndView("viewitems");
        List<Item>  items= itemRepository.findAll();
        modelAndView.addObject("items", items);

        return modelAndView;
    }*/

    @GetMapping("/items/add")
    @ResponseBody
    public ModelAndView addView(){
        return new ModelAndView("additemform");
    }

    @GetMapping("/items")
    @ResponseBody
    public RedirectView parse(@RequestParam(value = "url", required = true) String url, @RequestParam(value = "markup", required = false) Integer markup){
        Document doc = null;
        String title = "";
        Integer clean_price = 0;
        Integer sellig_price = 0;
        String content = "";
        int pr=0;
        String image = "";

        int maxLength = 255;
        Item item = null;

        try {
            doc = Jsoup.connect(url).get();
            Elements metaTags = doc.getElementsByTag("meta");

            for (Element metaTag : metaTags) {
                if (metaTag.hasAttr("property")){
                    String property = metaTag.attr("property");
                    String value = metaTag.attr("content");
                    if(property.equals("og:image")) {
                        image = value;
                    }
                    else if(property.equals("og:title")) {
                        pr = Integer.valueOf(value.split("\\.")[0]);
                    }
                }
                if(metaTag.hasAttr("name")){
                    String name = metaTag.attr("name");
                    String value = metaTag.attr("content");
                    if(name.equals("description"))
                        if(value.length() < 255)
                            content = value;
                        else
                            content = value.substring(0, 255);
                }
            }

            title = doc.title();
            clean_price = pr * 6;
            if(markup!=null){
                sellig_price = clean_price * (100 + markup) / 100;
            }else{
                sellig_price = clean_price;
            }

            item = new Item(title, content, image, clean_price, sellig_price);
            item = itemRepository.save(item);

        }catch (IOException e){
            e.printStackTrace();
        }

        return new RedirectView("/dashboard");
    }


    @GetMapping("/profile")
    @ResponseBody
    public ModelAndView get_profile() {
        return new ModelAndView("profile");
    }

    @GetMapping("/items/edit/{id}")
    @ResponseBody
    public ModelAndView updateView(@PathVariable(value = "id") Long itemId){
        ModelAndView modelAndView = new ModelAndView("editform");
        Item item = itemRepository.findById(itemId)
                .orElseThrow(() -> new ResourceNotFoundException("Item", "id", itemId));
        modelAndView.addObject("item", item);

        return modelAndView;
    }

    @ModelAttribute
    @PostMapping("/items/edit/{id}")
    public RedirectView updateItem(@PathVariable(value = "id") Long itemId,
                                   @ModelAttribute("item") Item itemDetails) {

        Item item = itemRepository.findById(itemId)
                .orElseThrow(() -> new ResourceNotFoundException("Item", "id", itemId));

        item.setTitle(itemDetails.getTitle());
        item.setContent(itemDetails.getContent());
        item.setCleanPrice(itemDetails.getCleanPrice());
        item.setSellingPrice(itemDetails.getSellingPrice());
        item.setImage(itemDetails.getImage());

        Item updatedItem = itemRepository.save(item);
        return new RedirectView("/dashboard");
    }

    @GetMapping("/items/delete/{id}")
    @ResponseBody
    public RedirectView deleteItem(@PathVariable(value = "id") Long itemId) {
        Item item = itemRepository.findById(itemId)
                .orElseThrow(() -> new ResourceNotFoundException("Item", "id", itemId));

        itemRepository.delete(item);

        return new RedirectView("/dashboard");
    }
}
