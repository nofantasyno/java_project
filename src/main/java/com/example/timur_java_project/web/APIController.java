package com.example.timur_java_project.web;

import com.example.timur_java_project.exception.ResourceNotFoundException;
import com.example.timur_java_project.model.Item;
import com.example.timur_java_project.repository.ItemRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@Controller
@RequestMapping("/api")
public class APIController {
    private ItemRepository itemRepository;

    @Autowired
    public APIController(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }


    @GetMapping("/products")
    @ResponseBody
    public Page getAllItems(@RequestParam(value = "page", required = false, defaultValue = "0") int page, @RequestParam(value = "sort", required = false, defaultValue = "createdAt") String sort) {
        Pageable pageable = PageRequest.of(page, 1000, Sort.by(sort));
        Page<Item> items =  itemRepository.findAll(pageable);
        return items;
    }

    @PostMapping("/products/create")
    @ResponseBody
    public Item parse(@RequestParam(value = "url", required = true) String url, @RequestParam(value = "markup", required = false) Integer markup){
        Document doc = null;
        String title;
        Integer clean_price = 0;
        Integer sellig_price = 0;
        String content;
        int pr=0;
        String image;

        Item item = null;

        try {
            doc = Jsoup.connect(url).get();
            title = doc.select("#j-product-detail-bd > div.detail-main > div > h1").get(0).text();
            pr = Integer.valueOf(doc.select("#j-sku-discount-price").get(0).text().replaceAll("\\s+", "").split(",")[0]);
            clean_price = pr * 6;
            if(markup!=null){
                sellig_price = clean_price * (100 + markup) / 100;
            }else{
                sellig_price = clean_price;
            }
            //content = doc.select("#product-prop-10 > span.propery-des").get(0).text();
            content = doc.select(".product-property-list").get(0).text().substring(0, 255);
            image = doc.select(".item-sku-image > a > img").get(0).attr("abs:bigpic");
            item = new Item(title, content, image, clean_price, sellig_price);
            item = itemRepository.save(item);

        }catch (IOException e){
            e.printStackTrace();
        }

        return item;
    }


    @GetMapping("/products/{id}")
    @ResponseBody
    public Item getItemById(@PathVariable(value = "id") Long itemId) {
        return itemRepository.findById(itemId)
                .orElseThrow(() -> new ResourceNotFoundException("Item", "id", itemId));
    }

    @PutMapping("/products/edit/{id}")
    public Item updateItem(@PathVariable(value = "id") Long itemId,
                                   @RequestBody Item itemDetails) {

        Item item = itemRepository.findById(itemId)
                .orElseThrow(() -> new ResourceNotFoundException("Item", "id", itemId));

        item.setTitle(itemDetails.getTitle());
        item.setContent(itemDetails.getContent());
        item.setCleanPrice(itemDetails.getCleanPrice());
        item.setSellingPrice(itemDetails.getSellingPrice());
        item.setImage(itemDetails.getImage());

        Item updatedItem = itemRepository.save(item);
        return updatedItem;
    }

    @DeleteMapping("/products/delete/{id}")
    @ResponseBody
    public List<Item> deleteItem(@PathVariable(value = "id") Long itemId) {
        Item item = itemRepository.findById(itemId)
                .orElseThrow(() -> new ResourceNotFoundException("Item", "id", itemId));

        itemRepository.delete(item);

        return itemRepository.findAll();
    }
}
