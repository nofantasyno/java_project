package com.example.timur_java_project.repository;

import com.example.timur_java_project.model.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    Page<Item> findAll(Pageable pageable);

    //Page<Item> findByPrice(Integer price, Pageable pageable);

    //Slice<Item> findByFirstNameAndLastName(String firstName, String lastName, Pageable pageable);

}
